def optimized_data(template, data):
    parsed = [tuple(k for k in i if type(k).__name__ == 'str' and k != '') for i in string.Formatter().parse(template)]
    dict_keys = [[i for i in re.split(r'[\[\]]', element[-1]) if i != ''] for element in parsed]
    result = {}

    def val(keys, data):
        result = {}
        for key in keys:
            if isinstance(data[key], dict):
                data = data[key]
                continue
            else:
                result = data[key]
        for key in reversed(keys):
            result = {key: result}
        return result

    def merge(source, destination):
        for key, value in source.items():
            if isinstance(value, dict):
                node = destination.setdefault(key, {})
                merge(value, node)
            else:
                destination[key] = value
        return destination

    for keys in dict_keys:
        result = merge(result, val(keys, data))
    return result


print(optimized_data(template, data))